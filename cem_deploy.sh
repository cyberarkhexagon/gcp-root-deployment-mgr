#!/usr/bin/env bash
set -e   # set -o errexit
set -u   # set -o nounset
set -o pipefail
[ "x${DEBUG:-}" = "x" ] || set -x

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

PROJECT_ID=
ROOT_ID=
CEM_DATASET_NAME=
DEPLOYMENT_EXIST=
CEM_DEFAULT_DATASET_NAME=cem_aggregated_logs_dataset
SERVICE_ACCOUNT_NAME=cem-service-account
SERVICE_ACCOUNT_ID=cyb-cem
CEM_SINK_NAME=cem-aggregated-sink
SERVICE_ACCOUNT_CONFIG_FILE=cem_service_account.yml
CUSTOM_CEM_ROLE_CONFIG_FILE=cem_custom_role_config.yml

function Setup
{
    # Get user input
    while getopts p:r:d:h:hs o
    do  case "$o" in
      p)  PROJECT_ID="$OPTARG";;
      r)  ROOT_ID="$OPTARG";;
      d)  CEM_DATASET_NAME="$OPTARG";;
      [?] | h) ShowUsage ; exit 1;;
      esac
    done

    if [[ "${PROJECT_ID}" == "PROJECT_ID" || "${ROOT_ID}" == "ROOT_ID" ]]; then
      ShowUsage
      exit 1
    fi

    if [[ -z "${PROJECT_ID}" || -z "${ROOT_ID}" ]]; then
      ShowUsage
      exit 1
    fi

    # Set cloud shell project to project_id
    gcloud config set project ${PROJECT_ID} > /dev/null 2>&1

    # Get deployment status
    DEPLOYMENT_EXIST=false
    for line in $(gcloud deployment-manager deployments list); do
        if [[ "${line}" == "cem-service-account" ]]; then
          DEPLOYMENT_EXIST=true
        fi
    done

    # Check dataset input
    if [[ -n $CEM_DATASET_NAME ]] && [[ $CEM_DATASET_NAME != $CEM_DEFAULT_DATASET_NAME  ]]; then
      bq show $CEM_DATASET_NAME > /dev/null 2>&1 || { echo "Dataset with the name $CEM_DATASET_NAME doesn't exist"; exit 1; }
    fi
}

function ShowUsage
{
    echo "Usage: $0 -p PROJECT_ID -r ROOT_ID"
}

function EnableAPIs
{
    echo "Enabling deploymentmanager, IAM ,cloudresourcemanager and bigQuery APIs..."
    gcloud services enable deploymentmanager.googleapis.com \
    cloudresourcemanager.googleapis.com \
    iam.googleapis.com \
    bigquery.googleapis.com \
    recommender.googleapis.com \
    policyanalyzer.googleapis.com
}

function AddPermissionsGoogleServiceAccount
{
    echo "Adding google serviceaccount permissions..."
    gserviceaccount=$(gcloud projects get-iam-policy \
    ${PROJECT_ID} | grep -m 1 -Po 'serviceAccount:[0-9]+@cloudservices.gserviceaccount.com')

    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member ${gserviceaccount} \
    --role roles/iam.securityAdmin > /dev/null 2>&1

    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member ${gserviceaccount} \
    --role roles/iam.roleAdmin > /dev/null 2>&1
}

function CreateOrUpdateCustomCEMRole
{
    # Create CustomCemRole and in case it exists update CustomCemRole
    gcloud iam roles undelete CustomCEMRole --organization=${ROOT_ID} > /dev/null 2>&1 \
     || echo "CustomCEMRole was enabled or doesn't exist"

    gcloud iam roles create CustomCEMRole --organization=${ROOT_ID} --file=${CUSTOM_CEM_ROLE_CONFIG_FILE} > /dev/null 2>&1 \
    || gcloud iam roles update CustomCEMRole --organization=${ROOT_ID} --file=${CUSTOM_CEM_ROLE_CONFIG_FILE} --quiet
}

function CreateOrUpdateDeployment
{
    if [ $DEPLOYMENT_EXIST = true ] ; then
      echo "Update an existing deployment..."
      gcloud deployment-manager deployments update ${SERVICE_ACCOUNT_NAME} --config ${SERVICE_ACCOUNT_CONFIG_FILE}
    else
      echo "Creating CEM service account deployment..."
      gcloud deployment-manager deployments create ${SERVICE_ACCOUNT_NAME} --config ${SERVICE_ACCOUNT_CONFIG_FILE}
    fi

    echo "Adding CEM service account to organization and bind it to CustomCemRole"
    gcloud organizations add-iam-policy-binding ${ROOT_ID} \
    --member=serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT_ID}.iam.gserviceaccount.com \
    --role="organizations/${ROOT_ID}/roles/CustomCEMRole" \
    --condition=None

    echo "Add role to CEM service account"
    gcloud organizations add-iam-policy-binding ${ROOT_ID} \
    --member=serviceAccount:${SERVICE_ACCOUNT_ID}@${PROJECT_ID}.iam.gserviceaccount.com \
    --role roles/bigquery.jobUser \
    --condition=None
}

function CreateBigQuery
{
    if [[ -z $CEM_DATASET_NAME ]] || [[ $CEM_DATASET_NAME == $CEM_DEFAULT_DATASET_NAME ]]; then
       CEM_DATASET_NAME=$CEM_DEFAULT_DATASET_NAME
       # Create bigQuery dataset
       echo "Creating bigQuery dataset..."
       bq mk ${PROJECT_ID}:${CEM_DATASET_NAME} >/dev/null 2>&1 || echo "$CEM_DATASET_NAME already exists."
    fi
}

function CreateSink
{
    #Creating an aggregated sink
    echo "Creating an aggregated sink"
    gcloud logging sinks create \
    ${CEM_SINK_NAME} bigquery.googleapis.com/projects/${PROJECT_ID}/datasets/${CEM_DATASET_NAME} --include-children \
    --organization=${ROOT_ID} \
    --log-filter='protoPayload.@type ="type.googleapis.com/google.cloud.audit.AuditLog" AND NOT resource.type: k8s' \
    --quiet  > /dev/null 2>&1 || echo "${CEM_SINK_NAME} already exists"

    cemsinkservice=$(gcloud beta logging sinks describe ${CEM_SINK_NAME} --organization=${ROOT_ID} |grep -m 1 -Po 'o[0-9]+-[0-9]+' )
    #Set IAM permission to sink service account
    echo "Adding sink serviceaccount permissions..."
    gcloud organizations add-iam-policy-binding ${ROOT_ID} \
    --member serviceAccount:${cemsinkservice}@gcp-sa-logging.iam.gserviceaccount.com \
    --role roles/bigquery.dataEditor \
    --condition=None

    #Set IAM permission to sink service account
    gcloud organizations add-iam-policy-binding ${ROOT_ID} \
    --member serviceAccount:${cemsinkservice}@gcp-sa-logging.iam.gserviceaccount.com \
    --role roles/logging.logWriter \
    --condition=None

    #Update sink with the new roles
    echo "Update sink..."
    gcloud logging sinks update  ${CEM_SINK_NAME} bigquery.googleapis.com/projects/${PROJECT_ID}/datasets/${CEM_DATASET_NAME} \
    --organization=${ROOT_ID}
}

function GetFinalValue
{
    echo Trying to get the final value
    manifest=$(gcloud deployment-manager deployments describe $SERVICE_ACCOUNT_NAME|grep -m 1 -Po 'manifest-[0-9]+')
    finalValue=$(gcloud deployment-manager manifests describe $manifest --deployment $SERVICE_ACCOUNT_NAME | grep finalValue|cut -d":" -f2 |cut -d" " -f2)
    echo Your final value is:
    echo $finalValue
}

function RunDeploymentSteps
{
    EnableAPIs
    AddPermissionsGoogleServiceAccount
    CreateOrUpdateCustomCEMRole
    CreateOrUpdateDeployment
    CreateBigQuery
    CreateSink
}

Setup "$@"
RunDeploymentSteps
GetFinalValue
